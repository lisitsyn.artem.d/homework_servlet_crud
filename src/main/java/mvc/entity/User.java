package mvc.entity;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@Entity
@Table(name = "users")
@NamedQuery(name = "getAllUsers", query = "from User")
@NamedQuery(name = "getUser", query = "select usr from User usr where usr.id = :id")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotBlank(message = "fullname can not be empty!")
    private String fullname;

    @Email
    @NotBlank(message = "email can not be empty!")
    private String email;

    @NotBlank(message = "address can not be empty!")
    private String address;

    @Pattern(regexp = "\\d{11}",message = "write phone number in appropriate way")
    @NotBlank(message = "phone can not be empty!")
    private String phone;

    @Pattern(regexp = "^((19|2[0-9])[0-9]{2})-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])$",message = "write date of birth in appropriate way")
    @NotBlank(message = "date of birth can not be empty!")
    private String date_of_birth;

    public User(){
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDate_of_birth() {
        return date_of_birth;
    }

    public void setDate_of_birth(String date_of_birth) {
        this.date_of_birth = date_of_birth;
    }
}
