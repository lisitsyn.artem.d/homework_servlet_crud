package mvc.dao;

import mvc.entity.User;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserDAO {
    public List<User> getAllUsers();
    public User getUser(int id);
    public void saveUser(User user);
    public void deleteUser(int id);
}
