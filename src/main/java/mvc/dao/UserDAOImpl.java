package mvc.dao;

import mvc.entity.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class UserDAOImpl implements UserDAO{

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List<User> getAllUsers() {
        Session session = sessionFactory.getCurrentSession();
        List<User> allUsers = session.createNamedQuery("getAllUsers",User.class).getResultList();
        return allUsers;
    }

    @Override
    public User getUser(int id) {
        Session session = sessionFactory.getCurrentSession();
        Optional<User> user = session.createNamedQuery("getUser",User.class)
                .setParameter("id",id)
                .getResultList()
                .stream()
                .findFirst();
        return user.get();
    }

    @Override
    public void saveUser(User user) {
        Session session = sessionFactory.getCurrentSession();
        session.saveOrUpdate(user);
    }

    @Override
    public void deleteUser(int id) {
        Session session = sessionFactory.getCurrentSession();
        session.delete(getUser(id));
    }
}
