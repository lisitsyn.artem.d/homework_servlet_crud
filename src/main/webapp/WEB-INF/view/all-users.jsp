<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<body>
<h3>All Users</h3>
<div>
    <table border="1px">
        <tr>
            <th>Fullname</th>
            <th>Email</th>
            <th>Phone</th>
            <th>Address</th>
            <th>Date of birth</th>
        </tr>
        <c:forEach var="user" items="${allUsers}">
            <c:url var="updateButton" value="/updateUser">
                <c:param name="userId" value="${user.id}"/>
            </c:url>
            <c:url var="deleteButton" value="/deleteUser">
                <c:param name="userId" value="${user.id}"/>
            </c:url>
            <tr>
                <td>${user.fullname}</td>
                <td>${user.email}</td>
                <td>${user.phone}</td>
                <td>${user.address}</td>
                <td>${user.date_of_birth}</td>
                <td>
                    <input type="button" value="Update" onclick="window.location.href = '${updateButton}'"/>
                    <input type="button" value="Delete" onclick="window.location.href = '${deleteButton}'"/>
                </td>
            </tr>
        </c:forEach>
    </table>
    <div>
        <input type="button" value="add" onclick="window.location.href = 'addUser'"/>
    </div>
</div>
</body>
</html>
