<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<body>
<h3>User info</h3>
<div>
    <form:form action="saveUser" modelAttribute="user">
        <form:hidden path="id"/>
        <div>
            <div>Fullname<form:input path="fullname"/></div>
            <div><form:errors path="fullname"/></div>
        </div>
        <div>
            <div>Email <form:input path="email"/></div>
            <div><form:errors path="email"/></div>
        </div>
        <div>
            <div>Phone <form:input path="phone"/></div>
            <div><form:errors path="phone"/></div>
        </div>
        <div>
            <div>Address <form:input path="address"/></div>
            <div><form:errors path="address"/></div>
        </div>
        <div>
            <div>Date of birth (YYYY-MM-DD) <form:input path="date_of_birth" /></div>
            <div><form:errors path="date_of_birth"/></div>
        </div>


        <div><input type="submit" value="OK"></div>
    </form:form>
</div>
</body>
</html>